# Deconstructable

This gem provides Deconstructable, a mixin module that helps you to support
pattern-matching over your types.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'deconstructable'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install deconstructable


## Usage

```
class Thing
    include Deconstructable

    ...

    # Mark some methods as deconstructable
    deconstructable :x, :y

    # Shortcut for attr_reader + deconstructable
    attr_deconstructable :readable_and_matchable

    # Using deconstructable as a method modifier
    deconstructable def foo
      do_the_foo
    end
end
```

This class provides a single DSL method `deconstructable` which helps you to mark
methods and attributes as deconstructable. Deconstructable attributes will be made
available in pattern matching, e.g.: 

```
thing in Thing(foo:, x: 100, y:)
```

Classes that include `Deconstructable` gain an implementation of `deconstruct_keys` that permits
hash-style key based pattern matching. Positional array-style patterns are not supported.
All deconstructable methods must support being called with no parameters.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run
`rake spec` to run the tests. You can also run `bin/console` for an interactive
prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To
release a new version, update the version number in `version.rb`, and then run
`bundle exec rake release`, which will create a git tag for the version, push
git commits and tags, and push the `.gem` file to
[rubygems.org](https://rubygems.org).

This Gem requires the new pattern matching features introduced in Ruby 2.7.0, so
we need at least that version. I use `asdf`, and have supplied a suitable
`.tool-versions` file. You can use this, or any other Ruby version manager.

## Contributing

Bug reports and pull requests are welcome on GitLab at
https://gitlab.com/alexkalderimis/deconstructable. This project is intended to
be a safe, welcoming space for collaboration, and contributors are expected to
adhere to the [code of conduct](https://gitlab.com/alexkalderimis/deconstructable/-/blob/master/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Deconstructable project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/alexkalderimis/deconstructable/-/blob/master/CODE_OF_CONDUCT.md).
