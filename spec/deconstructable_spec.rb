# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Deconstructable do
  it 'has a version number' do
    expect(Deconstructable::VERSION).not_to be nil
  end

  describe 'attr_deconstructable' do
    class TestClass
      include Deconstructable

      attr_deconstructable :foo, :bar, :baz

      def initialize(foo, bar, baz)
        @foo, @bar, @baz = foo, bar, baz
      end
    end

    it 'makes attributes readable and deconstructable' do
      thing = TestClass.new(:a, :b, :c)

      thing in TestClass(foo:, bar:, baz:)

      expect([foo, bar, baz]).to eq(%i[a b c])
      expect([foo, bar, baz]).to eq([thing.foo, thing.bar, thing.baz])
    end
  end

  describe 'decription example' do
    class Thing
      include Deconstructable

      attr_reader :x, :y
      deconstructable :x, :y

      def initialize(x, y)
        @x = x
        @y = y
      end

      deconstructable def foo
        x * y
      end
    end

    describe 'Thing(foo:, x: 100, y:)' do
      it 'binds foo and y if x == 100' do
        thing = Thing.new(100, 3)

        thing in Thing(foo:, x: 100, y:)

        expect(y).to eq(3)
        expect(foo).to eq(300)
      end
    end
  end
end
