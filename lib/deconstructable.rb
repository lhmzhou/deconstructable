# frozen_string_literal: true

# Helpers to implement pattern match-able classes
module Deconstructable
  def self.included(base)
    base.extend ClassMethods
  end

  # DSL methods made available to the including class:
  module ClassMethods
    # Method names that represent attributes that should be exposed in patterns
    def deconstructable(*keys)
      @deconstructable ||= (superclass&.instance_variable_get(:@deconstructable)&.dup || [])
      @deconstructable.concat(keys)
    end

    # Sugar defining readable attributes (as per `attr_reader`) that are
    # deconstructable
    def attr_deconstructable(*keys)
      attr_reader(*keys)
      deconstructable(*keys)
    end
  end

  def deconstruct_keys(keys)
    keys = keys ? (deconstructable & keys) : deconstructable

    keys.map { |k| [k, send(k)] }.to_h
  end

  private

  def deconstructable
    self.class.deconstructable
  end
end
