# frozen_string_literal: true

require_relative 'lib/deconstructable/version'

SUMMARY = <<~SUMMARY
  Mixin to help implement deconstructable types
SUMMARY

DESCRIPTION = <<~DESC
  This gem provides Deconstructable, a mixin module that helps you to support
  pattern-matching over your types.

  Usage
  --------

  ```
  class Thing
    include Deconstructable

    ...

    deconstructable :x, :y

    deconstructable def foo
      do_the_foo
    end
  end
  ```

  This class provides a single DSL method `deconstructable` which helps you to mark
  methods and attributes as deconstructable. Deconstructable attributes will be made
  available in pattern matching, e.g.:

  ```
  thing in Thing(foo:, x: 100, y:)
  ```

  Classes that include `Deconstructable` gain an implementation of `deconstruct_keys` that permits
  hash-style key based pattern matching. Positional array-style patterns are not supported.
DESC

Gem::Specification.new do |s|
  s.name        = 'deconstructable'
  s.version     = Deconstructable::VERSION
  s.date        = '2020-04-07'
  s.summary     = SUMMARY
  s.description = DESCRIPTION
  s.authors     = ['Alexis Kalderimis']
  s.email       = ['alex.kalderimis@gmail.com']
  s.homepage    = 'https://rubygems.org/gems/deconstructable'
  s.license     = 'MIT'
  s.required_ruby_version = '>= 2.7.0'

  s.metadata['homepage_uri'] = s.homepage
  s.metadata['source_code_uri'] = 'https://gitlab.com/alexkalderimis/deconstructable'
  s.metadata['changelog_uri'] = 'https://gitlab.com/alexkalderimis/deconstructable/-/blob/master/CHANGELOG.md'

  s.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      f.match(%r{^(test|spec|features)/})
    end
  end
  s.require_paths = ['lib']

  s.add_development_dependency 'rspec', '~> 3.2'
end
